package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.GameManager;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/create")
public class CreateGameController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    Player player;
    GameManager gameListService = GameListService.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        player = (Player) request.getSession().getAttribute("Player");
        gameListService.createGame(player);
        request.setAttribute("Player", player);
        response.sendRedirect("list");
    }

}

