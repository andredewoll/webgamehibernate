package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.EncryptionService;
import com.softserveinc.ita.multigame.services.PlayerHiberService;
import com.softserveinc.ita.multigame.services.PlayerManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/update")
public class EditInfoController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    PlayerManager playerManager = new PlayerHiberService();
    Player player;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        player = (Player) request.getSession().getAttribute("Player");
        String email = request.getParameter("email");
        player.setEmail(email);
        String fullName = request.getParameter("fullName");
        if (!fullName.equals("")) {
            player.setFullName(fullName);
        }
        String gender = request.getParameter("gender");
        if (gender != null) {
            player.setGender(Gender.valueOf(gender));
        } else {
            player.setGender(null);
        }
        String date = request.getParameter("date");
        if (date.length() != 0) {
            int year = Integer.parseInt(date.substring(0, 4));
            int month = Integer.parseInt(date.substring(5, 7));
            int day = Integer.parseInt(date.substring(8, 10));
            player.setBirthdayDate(LocalDate.of(year, month, day));
        }
        String about = request.getParameter("about");
        if (!about.equals("")) {
            player.setAbout(about);
        }

        playerManager.updatePlayer(player);
        player = playerManager.getPlayerByLogin(player.getLogin());
        request.getSession().setAttribute("Player", player);

        response.sendRedirect("profile?name=" + player.getLogin());

    }
}
