package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.Turn;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.GameManager;
import com.softserveinc.ita.multigame.services.ToJsonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/gameHistory/*")
public class GameHistoryController extends HttpServlet {

    GameManager gameManager = GameListService.getInstance();
    GameHistory gameHistory;
    Player firstPlayer, secondPlayer;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String winner =(String) request.getSession().getAttribute("winner");
        Long id = Long.parseLong(request.getParameter("id"));
        gameHistory = gameManager.getGameHistory(id);
        firstPlayer = gameHistory.getFirstPlayer();
        secondPlayer = gameHistory.getSecondPlayer();

        List<String> turnList = new ArrayList<>();
        for (String t : gameHistory.getTurnList()) {
            Turn turn = (Turn) ToJsonService.fromJson(t, Turn.class);
            Player p;
            if (turn.getPlayerId().equals(firstPlayer.getId())) {
                p = firstPlayer;
            } else {
                p = secondPlayer;
            }
           turnList.add(String.format("turn: %s\t player: %s\t date: %s",
                    turn.getTurn(), p.getLogin(), turn.getTime()));
        }

        request.setAttribute("id", gameHistory.getId());
        request.setAttribute("firstPlayer", firstPlayer.getLogin());
        request.setAttribute("secondPlayer", secondPlayer.getLogin());
        request.setAttribute("winner", winner);
        request.setAttribute("turnList", turnList);
        request.getRequestDispatcher("/History.jsp").forward(request, response);
    }


}
