package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.EncryptionService;
import com.softserveinc.ita.multigame.services.PlayerHiberService;
import com.softserveinc.ita.multigame.services.PlayerManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    PlayerManager playerService = new PlayerHiberService();
    EncryptionService encryptionService = EncryptionService.getInstance();
    Player player;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("name");
        String password = request.getParameter("password");
        player = playerService.getPlayerByLogin(login);

        if( player != null && encryptionService.checkPassword(password, player.getPassword())) {
            request.getSession().setAttribute("Player", player);
            request.getSession().setAttribute("name", login);
            response.sendRedirect("list");
        } else {
            request.setAttribute("error", "Please, register, You are not in our DB yet!");
            request.getRequestDispatcher("/login.html").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.html").forward(req, resp);
    }
}
