package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.GameManager;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/list")
public class GameListController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    GameManager gameService = GameListService.getInstance();
    Player player;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        player = (Player) request.getSession().getAttribute("Player");
        String login = (String) request.getSession().getAttribute("name");
        request.setAttribute("name", login);
        request.setAttribute("listOfCreatedGames", gameService.getCreatedGamesIds(player));
        request.setAttribute("listOfWaitingGames", gameService.getWaitingGamesIds(player));
        request.setAttribute("listOfPlayingGames", gameService.getPlayingGamesIds(player));
        request.getRequestDispatcher("/ListOfGames.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }


}
