package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/")
public class AuthFilter implements Filter{
    private static final long serialVersionUID = 2L;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp= (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        String servletPath = req.getServletPath();

        if (servletPath.equals("/login")) {
           chain.doFilter(req, resp);
            return;
        }

        if (servletPath.equals("/registration")) {
            chain.doFilter(req, resp);
            return;
        }

        Player player = (Player) req.getSession().getAttribute("player");
        if (player != null) {
            chain.doFilter(req, resp);
            return;
        }
        String url = req.getContextPath() + "/login";
        resp.sendRedirect(url);

    }

    @Override
    public void destroy() {
    }
}
