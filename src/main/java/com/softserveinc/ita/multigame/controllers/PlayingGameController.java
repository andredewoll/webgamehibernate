package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.RockScissorsPaper;
import com.softserveinc.ita.multigame.services.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/play/*")
public class PlayingGameController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private GameManager gameService = GameListService.getInstance();
  //  private TurnService turnService = new TurnService();
    private PlayerManager playerService = new PlayerHiberService();
   // private Game game;
    private Player player;
    private RockScissorsPaper rsp = null;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long gameId =Long.parseLong(request.getParameter("id"));
        String currentPlayer =(String) request.getSession().getAttribute("name");
        rsp = gameService.getGameById(gameId).getGameEngine();
        //player = playerService.getPlayerByLogin(currentPlayer);

        String enemy ;
        if (gameService.getGameById(gameId).getFirstPlayer().getLogin().equals(currentPlayer)) {
            enemy = gameService.getGameById(gameId).getSecondPlayer().getLogin();
        } else enemy = gameService.getGameById(gameId).getFirstPlayer().getLogin();

        if (gameService.getGameById(gameId).getSecondPlayer() == null) {
            gameService.getGameById(gameId).setSecondPlayer(playerService.
                    getPlayerByLogin(currentPlayer));
        } else {
            gameService.getGameById(gameId).
                    setSecondPlayer(playerService.getPlayerByLogin(enemy));
        }

        rsp.setFirstPlayer(currentPlayer);
        rsp.setSecondPlayer(enemy);

        boolean isStarted = gameService.getGameById(gameId).getGameEngine().isStarted();
        int resultCode = gameService.getGameById(gameId).getGameEngine().getResultCode();
        String gameState = gameService.getGameById(gameId).getGameEngine().toString();
        request.setAttribute("gameState", gameState);
        request.setAttribute("isStarted", isStarted);
        request.setAttribute("resultCode", resultCode);
        request.setAttribute("id", gameId);
        request.setAttribute("currentPlayerName", currentPlayer);
        request.setAttribute("enemy", enemy);


        if (gameService.getGameById(gameId).getGameEngine().isFinished()) {
            request.setAttribute("winner", gameService.getGameById(gameId).getGameEngine().getTheWinner());
            gameService.deleteGame(gameId);

        }
        getServletContext().getRequestDispatcher("/PlayingGames.jsp").forward(request, response);
        }


    }




