package com.softserveinc.ita.multigame.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/created/*")
public class CreatedGamesController extends HttpServlet {
    private static final long serialVersionUID = 2L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = (String) request.getSession().getAttribute("id");
        String login = (String) request.getSession().getAttribute("name");

        request.setAttribute("id", id);
        request.setAttribute("name", login );

        request.getRequestDispatcher("/CreatedGames.jsp").forward(request, response);
    }
}
