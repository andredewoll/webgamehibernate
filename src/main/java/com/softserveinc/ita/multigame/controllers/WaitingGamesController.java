package com.softserveinc.ita.multigame.controllers;

        import com.softserveinc.ita.multigame.model.Player;
        import com.softserveinc.ita.multigame.services.GameListService;
        import com.softserveinc.ita.multigame.services.GameManager;

        import javax.servlet.RequestDispatcher;
        import javax.servlet.ServletContext;
        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;


@WebServlet("/Waiting/*")
public class WaitingGamesController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    GameManager gameListServiceService = GameListService.getInstance();
    Player player;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        String enemy = gameListServiceService.getGameById(id).getFirstPlayer().getLogin();
        player = (Player) request.getSession().getAttribute("player");
        request.setAttribute("id", request.getParameter("id"));
        request.setAttribute("name", request.getParameter("name"));
        request.setAttribute("enemy", enemy);
        request.setAttribute("Player", player);

        request.getRequestDispatcher("/WaitingGames.jsp").forward(request, response);
    }

}
