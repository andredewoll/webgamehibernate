package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;

import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.GameManager;
import com.softserveinc.ita.multigame.services.PlayerHiberService;
import com.softserveinc.ita.multigame.services.PlayerManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile/*")
public class ProfileController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    PlayerManager playerService = new PlayerHiberService();

    Player player;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String enemy = request.getParameter("enemy");

        player = playerService.getPlayerByLogin(name);
        Player currentPlayer = (Player) request.getSession().getAttribute("Player");

        request.setAttribute("name", name);
        request.setAttribute("enemy", enemy);
        request.setAttribute("Player", player);

        if (player.equals(currentPlayer)){
            request.getRequestDispatcher("/myProfile.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/Profile.jsp").forward(request, response);
        }
    }
}
