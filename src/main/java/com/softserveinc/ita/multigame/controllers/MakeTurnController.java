package com.softserveinc.ita.multigame.controllers;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.*;
import org.apache.log4j.Logger;

import java.io.IOException;

@WebServlet("/MakeTurn")
public class MakeTurnController extends HttpServlet {
	static final Logger LOGGER = Logger.getLogger(MakeTurnController.class);
	private static final long serialVersionUID = 1L;

	GameManager gameService = GameListService.getInstance();
	Game game;
	Player player;
	PlayerManager playerService = new PlayerHiberService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String turn = request.getParameter("turn");
		String currentPlayerName = request.getParameter("currentPlayerName");
		Long id = Long.parseLong(request.getParameter("id"));

		String enemy;
		if (gameService.getGameById(id).getFirstPlayer().getLogin().equals(currentPlayerName)) {
			enemy = gameService.getGameById(id).getSecondPlayer().getLogin();
		} else {
			enemy = gameService.getGameById(id).getFirstPlayer().getLogin();
		}
		player = playerService.getPlayerByLogin(currentPlayerName);
		game = gameService.getGameById(id);
		game.makeTurn(player, turn);

		boolean isStarted = gameService.getGameById(id).getGameEngine().isStarted();
		String winner = gameService.getGameById(id).getGameEngine().getTheWinner();
		int resultCode = gameService.getGameById(id).getGameEngine().getResultCode();
		String gameState = gameService.getGameById(id).getGameEngine().toString();

		request.setAttribute("gameState", gameState);
		request.setAttribute("currentPlayerName", currentPlayerName);
		request.setAttribute("enemy", enemy);
		request.setAttribute("id", id);
		request.setAttribute("winner", winner);
		request.setAttribute("resultCode", resultCode);
		request.setAttribute("isStarted", isStarted);

		if (winner != null) {
			LOGGER.info("The winner is  " + winner);
			gameService.getGameById(id).getGameEngine().isFinished();
		}
		request.getRequestDispatcher("/PlayingGames.jsp").forward(request, response);
	}
}

