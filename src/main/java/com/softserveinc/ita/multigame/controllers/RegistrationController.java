package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.EncryptionService;
import com.softserveinc.ita.multigame.services.PlayerHiberService;
import com.softserveinc.ita.multigame.services.PlayerManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@WebServlet("/registration")
public class RegistrationController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    static final Logger LOGGER = Logger.getLogger(RegistrationController.class);

    PlayerManager playerService = new PlayerHiberService();
    EncryptionService encryptionService = EncryptionService.getInstance();
    Player player;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("name");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        player = new Player(login, encryptionService.encryptPassword(password), email);
        String fullName = request.getParameter("fullName");
        if (!fullName.equals("")) {
            player.setFullName(fullName);
        }
        String gender = request.getParameter("gender");
        if (gender != null) {
            player.setGender(Gender.valueOf(gender));
        } else {
            player.setGender(null);
        }
        String about = request.getParameter("about");
        if (!about.equals("")) {
            player.setAbout(about);
        }
        String date = request.getParameter("date");
        if (date.length() != 0) {
            int year = Integer.parseInt(date.substring(0, 4));
            int month = Integer.parseInt(date.substring(5, 7));
            int day = Integer.parseInt(date.substring(8, 10));

            player.setBirthdayDate(LocalDate.of(year, month, day));
        }

        LocalDateTime registrationTime = LocalDateTime.now();
        player.setRegistrationTime(registrationTime);

        boolean result = playerService.registerNewPlayer(player);
        LOGGER.info("Added player  " + login + "  " + password + "  " + email);
        request.getSession().setAttribute("Player", player);
        request.getSession().setAttribute("name", login);

        if (result) {
            response.sendRedirect("list");
        } else {
            request.getRequestDispatcher("/errorRegistration.html").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/registration.html").forward(req, resp);
    }
}

