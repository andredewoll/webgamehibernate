package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;
import com.softserveinc.ita.multigame.model.engine.RockScissorsPaper;
import com.softserveinc.ita.multigame.services.ToJsonService;
import com.sun.prism.impl.paint.PaintUtil;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Game {

	private Player firstPlayer;
	private Player secondPlayer;
	private RockScissorsPaper gameEngine;
	private Long id;
	private LocalDateTime start;
	private LocalDateTime endTime;
	private List<String> turnList;
	
	public Game() {
		super();
	}

	public Game (Player player) {
		gameEngine = new RockScissorsPaper();
		this.id = gameEngine.getId();
		gameEngine.setFirstPlayer(player.getLogin());
		firstPlayer = player;
		start = LocalDateTime.now();


		turnList = new ArrayList<>();
	}

	
	
	public Player getFirstPlayer() {
		return firstPlayer;
	}
	
	public void setFirstPlayer(Player firstPlayer) {
		if(gameEngine.getFirstPlayer()==null){
			gameEngine.setFirstPlayer(firstPlayer.getLogin());
			this.firstPlayer = firstPlayer;
		}
	}
	
	public Player getSecondPlayer() {
		return secondPlayer;
	}
	
	public void setSecondPlayer(Player secondPlayer) {
		if(gameEngine.getSecondPlayer()==null){
			gameEngine.setSecondPlayer(secondPlayer.getLogin());
			this.secondPlayer = secondPlayer;
		}
	}
	
	public RockScissorsPaper getGameEngine() {
		return gameEngine;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		if(start == null && (gameEngine.getFirstPlayer() != null && gameEngine.getSecondPlayer() != null)) {
			start = LocalDateTime.now();
		}
		this.start = start;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public List<String> getTurnList() {
		return turnList;
	}

	public void setTurnList(List<String> turnList) {
		this.turnList = turnList;
	}

	public boolean isFinished() {
		return this.gameEngine.isFinished();
	}

	public boolean isPlayed() {return  this.gameEngine.isStarted() && !this.gameEngine.isFinished(); }

	public boolean makeTurn(Player player, String turn){
		saveTurn(player, turn);
		return gameEngine.makeTurn(player.getLogin(), turn);
	}

	private void saveTurn(Player player, String turn) {
		Turn turnHistory = new Turn(LocalDateTime.now(), player.getLogin(), gameEngine.getId(), turn);
		String gSonTurn = ToJsonService.makeJson(turnHistory);
		turnList.add(gSonTurn);
	}

	public ResultStatus getResultStatus() {

		if (gameEngine.getTheWinner().equals(firstPlayer.getLogin())){
			return ResultStatus.FIRST_IS_WINNER;
		}
		if (gameEngine.getTheWinner().equals(secondPlayer.getLogin())){
			return ResultStatus.SECOND_IS_WINNER;
		}
		return ResultStatus.DRAW;
	}


	

}
