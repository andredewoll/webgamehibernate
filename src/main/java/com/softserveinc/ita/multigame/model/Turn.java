package com.softserveinc.ita.multigame.model;

import java.time.LocalDateTime;


public class Turn {
    private LocalDateTime time;
    private String playerLogin;
    private Long playerId;
    private Long gameId;
    private String turn;

    public Turn() {}

    public Turn(LocalDateTime time, String playerLogin, Long gameId, String turn) {
        this.time = time;
        this.playerLogin = playerLogin;
        this.gameId = gameId;
        this.turn = turn;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getPlayerLogin() {
        return playerLogin;
    }

    public Long getGameId() {
        return gameId;
    }

    public String getTurn() {
        return turn;
    }

    public Long getPlayerId() {
        return playerId;
    }

}
