package com.softserveinc.ita.multigame.model;


public enum ResultStatus {
    FIRST_IS_WINNER, SECOND_IS_WINNER, DRAW
}
