package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.util.UtilSessionFactory;
import com.softserveinc.ita.multigame.dao.util.UtilSessionFactoryImpl;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameHistoryDaoImpl implements GameHistoryDao {
    static final Logger LOGGER = Logger.getLogger(GameHistoryDaoImpl.class);
    private UtilSessionFactory sessionFactory = UtilSessionFactoryImpl.getInstance();

    @Override
    public boolean save(GameHistory gameHistory) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(gameHistory);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e){
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }


    @Override
    public boolean delete(GameHistory gameHistory) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(gameHistory);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e){
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }


    @Override
    public boolean update(GameHistory gameHistory) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.update(gameHistory);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }

    @Override
    public GameHistory getById(Long id) {
        //if (id == null) return null;
        Session session = sessionFactory.openSession();
        GameHistory gameHistory = session.get(GameHistory.class, id);
        session.close();
        return gameHistory;
    }

    @Override
    public List<GameHistory> getAllGames(Player player) {
        List<GameHistory> historyList = new ArrayList<>();
        for (Iterator<GameHistory> iterator = getBy1PlayerGames(player).iterator(); iterator.hasNext(); ) {
            Object o = iterator.next();
            historyList.add((GameHistory) o);
        }
        for (Iterator<GameHistory> iterator = getBy2PlayerGames(player).iterator(); iterator.hasNext(); ) {
            Object o = iterator.next();
            historyList.add((GameHistory) o);
        }
        return historyList;
    }

    @Override
    public List<GameHistory> getBy1PlayerGames(Player player) {
        if (player == null) return null;
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(GameHistory.class);
        Criteria anotherCriteria = criteria.createCriteria("firstPlayer");
        anotherCriteria.add(Restrictions.eq("id", player.getId()));
        List<GameHistory> histories = criteria.list();
        session.close();
        return histories;
    }

    @Override
    public List<GameHistory> getBy2PlayerGames(Player player) {
        if (player == null) return null;
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(GameHistory.class);
        Criteria anotherCriteria = criteria.createCriteria("secondPlayer");
        anotherCriteria.add(Restrictions.eq("id", player.getId()));
        List<GameHistory> histories = criteria.list();
        session.close();
        return histories;
    }


}
