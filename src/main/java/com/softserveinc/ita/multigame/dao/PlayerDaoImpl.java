package com.softserveinc.ita.multigame.dao;


import com.softserveinc.ita.multigame.dao.util.UtilSessionFactory;
import com.softserveinc.ita.multigame.dao.util.UtilSessionFactoryImpl;
import com.softserveinc.ita.multigame.model.Player;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class PlayerDaoImpl implements PlayerDao {
    private UtilSessionFactory sessionFactory = UtilSessionFactoryImpl.getInstance();
    static final Logger LOGGER = Logger.getLogger(PlayerDaoImpl.class);

    @Override
    public boolean save(Player player) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.save(player);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }

    @Override
    public boolean update(Player player) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.merge(player);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }

    @Override
    public boolean delete(Player player) {
        Session session = sessionFactory.openSession();
        try {
            session.getTransaction().begin();
            session.delete(player);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            session.getTransaction().rollback();
            return false;
        }
    }

    @Override
    public Player getById(Long id) {
        if (id == null) return null;
        Session session = sessionFactory.openSession();
        Player p = session.get(Player.class, id);
        session.close();
        return p;
    }

    @Override
    public Player getByLogin(String login) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Player.class);
        criteria.add(Restrictions.eq("login", login));
        Player p = (Player) criteria.uniqueResult();
        session.close();
        return p;
    }

    @Override
    public List<Player> getAll() {
        Session session = sessionFactory.openSession();
        List<Player> players = session.createCriteria(Player.class).list();
        return players;
    }
}

