package com.softserveinc.ita.multigame.dao.util;

import org.hibernate.Session;

public interface UtilSessionFactory {
    Session openSession();
    void close();
}
