package com.softserveinc.ita.multigame.dao;


import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.util.List;


public interface GameHistoryDao {
    boolean save(GameHistory gameHistory);
    boolean delete(GameHistory gameHistory);
    boolean update(GameHistory gameHistory);
    GameHistory getById(Long id);
    List<GameHistory> getAllGames(Player player);
    List<GameHistory> getBy2PlayerGames(Player player);
    List<GameHistory> getBy1PlayerGames(Player player);

}
