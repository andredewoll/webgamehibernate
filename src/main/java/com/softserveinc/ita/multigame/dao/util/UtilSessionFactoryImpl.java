package com.softserveinc.ita.multigame.dao.util;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UtilSessionFactoryImpl implements UtilSessionFactory {
    private SessionFactory sessionFactory;
    private static UtilSessionFactoryImpl instance;

    private UtilSessionFactoryImpl() {
        sessionFactory = new Configuration().configure("db\\hibernate.cfg.xml").buildSessionFactory();
    }


    public static UtilSessionFactoryImpl getInstance() {
        if (instance == null) {
            instance = new UtilSessionFactoryImpl();
        } return instance;
    }

    @Override
    public Session openSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void close() {
        sessionFactory.close();
    }
}
