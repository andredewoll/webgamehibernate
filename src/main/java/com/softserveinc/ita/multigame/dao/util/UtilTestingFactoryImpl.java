package com.softserveinc.ita.multigame.dao.util;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;
import com.softserveinc.ita.multigame.model.Turn;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UtilTestingFactoryImpl implements UtilSessionFactory {
    private SessionFactory sessionFactory;
    private static UtilTestingFactoryImpl instance;

     UtilTestingFactoryImpl() {
        sessionFactory = new Configuration().configure("db/hibernateTest.cfg.xml").buildSessionFactory();
        fill();
    }


    public static UtilTestingFactoryImpl getInstance() {
        if (instance == null) {
            instance = new UtilTestingFactoryImpl();
        } return instance;
    }

    @Override
    public Session openSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void close() {
        sessionFactory.close();
    }

    public void fill() {
        Player p1 = new Player("Andre", "qwerty", "a@mail");
        Player p2 = new Player("Alex", "qwerty", "ax@mail");

        List<String> turns = new ArrayList<>();
        turns.add(String.format("time, player, gameId, turn   ",LocalDateTime.of(1999,1,2,11,12), "Andre", 1L, "ROCK"));
        turns.add(String.format("time, player, gameId, turn   ",LocalDateTime.of(1999,1,2,11,13), "Alex", 1L, "SCISSORS"));
        GameHistory gameHistory = new GameHistory(1L, p1, p2, LocalDateTime.now(), LocalDateTime.now(), turns, ResultStatus.FIRST_IS_WINNER);
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        session.save(p1);
        session.save(p2);
        session.save(gameHistory);
        session.getTransaction().commit();
        session.close();

    }
}
