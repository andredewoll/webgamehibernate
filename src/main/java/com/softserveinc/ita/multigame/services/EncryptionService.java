package com.softserveinc.ita.multigame.services;

import org.jasypt.util.password.StrongPasswordEncryptor;

public class EncryptionService {

    private static EncryptionService instance = null;

    private StrongPasswordEncryptor passwordEncryptor;

    private EncryptionService() {
        passwordEncryptor = new StrongPasswordEncryptor();
    }

    public static synchronized EncryptionService getInstance() {
        if (instance == null) {
            instance = new EncryptionService();
        }
        return instance;
    }


    public String encryptPassword(String password) {
        return passwordEncryptor.encryptPassword(password);
    }


    public boolean checkPassword(String originalPassword, String encryptedPasswordStrong) {
        return passwordEncryptor.checkPassword(originalPassword, encryptedPasswordStrong);
    }




}
