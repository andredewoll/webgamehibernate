package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;

import java.util.List;


public interface PlayerManager {
    boolean registerNewPlayer(Player player);
    Player getPlayerByLogin(String login);
    List<Player> getAllPlayers();
    Player getById(Long id);
    boolean updatePlayer (Player player);
    boolean deletePlayer (Player player);

}