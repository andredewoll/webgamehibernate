package com.softserveinc.ita.multigame.services;


import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.time.LocalDateTime;
import java.util.List;

public class PlayerHiberService implements PlayerManager {
    PlayerDao playerDao = new PlayerDaoImpl();

    @Override
    public boolean registerNewPlayer(Player player) {
        return playerDao.save(player);
    }
    @Override
    public Player getPlayerByLogin(String login) {
        return playerDao.getByLogin(login);
    }
    @Override
    public Player getById(Long id) {
        return playerDao.getById(id);
    }
    @Override
    public boolean updatePlayer(Player player) {
        return playerDao.update(player);
    }
    @Override
    public List<Player> getAllPlayers() {
        return playerDao.getAll();
    }
    @Override
    public boolean deletePlayer(Player player) {
       return playerDao.delete(player);
    }

}
