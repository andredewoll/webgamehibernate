package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GameListService implements GameManager {
    private static Map<Long, Game> map;

    private static GameListService instance = null;
    private PlayerDao playerDao = new PlayerDaoImpl();
    private GameHistoryDao gameHistoryDao = new GameHistoryDaoImpl();
    private Map<Long, Game> deletedGames = new ConcurrentHashMap<>();

    private GameListService() {
        map = new ConcurrentHashMap<>();
    }

    public static GameListService getInstance() {
        if (instance == null) {
            instance = new GameListService();
        }
        return instance;
    }


    public boolean createGame(Player player) {
        if (player == null) {
            return false;
        }
        Game game = new Game(playerDao.getByLogin(player.getLogin()));
        map.put(game.getGameEngine().getId(), game);
        return map.containsKey(game.getGameEngine().getId());
    }

    public boolean deleteGame(Long id) {
        Game game = map.remove(id);
        if (game != null) {
            game.setEndTime(LocalDateTime.now());
            saveGameHistory(game);
            deletedGames.put(game.getId(), game);
            return true;
        }

        return false;
    }


    public List<Long> getCreatedGamesIds(Player player) {
        List<Long> listCreated = new ArrayList<>();
        for (Map.Entry<Long, Game> entry : map.entrySet()) {

            if (entry.getValue().getFirstPlayer().equals(player)
                    && entry.getValue().getSecondPlayer() == null) {
                listCreated.add(entry.getKey());
            } //This means that me (1st) player created game and is waiting for 2nd (somebody else)
        }

        return listCreated;
    }

    public List<Long> getPlayingGamesIds(Player player) {
        List<Long> listCreated = new ArrayList<>();
        for (Map.Entry<Long, Game> entry : map.entrySet()) {

            if (entry.getValue().getFirstPlayer().equals(player)
                    && entry.getValue().getSecondPlayer() != null) {
                listCreated.add(entry.getKey());
            } //This means that 1st & 2nd players are playing, cause secondPlayer != null
        }

        return listCreated;
    }

    public List<Long> getWaitingGamesIds(Player player) {
        List<Long> listCreated = new ArrayList<>();
        for (Map.Entry<Long, Game> entry : map.entrySet()) {
            if (entry.getValue().getSecondPlayer() == null
                    && !entry.getValue().getFirstPlayer().equals(player)) {
                listCreated.add(entry.getKey());
            } //This means that somebody else (1st player) created game and waits for me (2nd) to join
        }
        return listCreated;
    }

    public Game getGameById(Long id) {
        if (map.containsKey(id)) {
            Game game = map.get(id);
            checkGameIsFinished(game);
            return game;
        } if (deletedGames.containsKey(id)){
            return deletedGames.get(id);
        }
        return null;

    }

    private void saveGameHistory(Game game) {
        GameHistory gh = new GameHistory(game.getId(), game.getFirstPlayer(), game.getSecondPlayer(),
                game.getStart(), game.getEndTime(), game.getTurnList(), game.getResultStatus());
        gameHistoryDao.save(gh);
    }

    private void checkGameIsFinished(Game game) {
        if (game.isFinished()){
            deleteGame(game.getId());
        }
    }


    public List<Long> getAllGames() {
        return new ArrayList<>(map.keySet());
    }

    @Override
    public GameHistory getGameHistory(Long gameId) {
        return gameHistoryDao.getById(gameId);
    }


}
