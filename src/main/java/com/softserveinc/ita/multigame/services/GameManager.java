package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.util.List;


public interface GameManager {

    boolean createGame(Player player);
    boolean deleteGame(Long id);
    Game getGameById(Long id);
    List<Long> getCreatedGamesIds(Player player);
    List<Long> getPlayingGamesIds(Player player);
    List<Long> getWaitingGamesIds(Player player);
    List<Long> getAllGames();
    GameHistory getGameHistory(Long gameId);
}
