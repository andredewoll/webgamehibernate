<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

</head>

<body>
<h1>Game#${id}</h1>


<h2>${firstPlayer} vs ${secondPlayer}</h2>

<h2>Winner is ${winner}</h2>
<textarea id="turns" >
<%
    for (String turn: (List<String>)request.getAttribute("turnList")){
        out.println(turn);
    }
%>
</textarea>
<div class="button_container">
    <form method="get" action="profile">
        <input type="hidden" name="name" value=${Player.getLogin()}>
        <input type="submit" class="w3-btn" value="Back">
    </form>
</div>

</body>
</html>
