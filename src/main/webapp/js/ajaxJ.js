$(document).ready(function() {

    makeTurn();
    setInterval(function() {
        refreshBoard()
    }, 1000); //2 seconds
});



function refreshBoard() {
    $.ajax({
        method: 'GET',
        url: 'play?id=' + $('#id').val()+'&name=' +$('#currentPlayerName').val() + '&enemy=' + ('#enemy').val(),
        success: function () {
         //   refreshBoard();
        }
    });
}

function makeTurn(){

    $("#turnForm").submit(function(e) {
        $.ajax({
            type: "POST",
            url: 'MakeTurn',
            data: $("#turnForm").serialize(),
            success: function(data) {
                $('.changes').val();
                //drawBoard(data.board);
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
}
