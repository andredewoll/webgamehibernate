'use strict';

window.addEventListener('load', main, false);

var changes, restart, ajax = false;

function main() {
    init();
    start();
}

function init() {
    changes = document.querySelectorAll('.changes');
}

function start() {
    restart = true;
    request();
}

function request() {
    ajax = new XMLHttpRequest();

    ajax.open('GET', 'play?id='+ $('#id').val() +'&name=' +$('#currentPlayerName').val() + '&enemy=' + $('#enemy').val(), true);
    ajax.send();

    ajax.addEventListener('readystatechange', function () {
        if (ajax.readyState < 4)
            return;

        if (ajax.status != 200) {
            errorResult();
        } else {
            successResult();
        }
    }, false);
}

function errorResult() {
    alert(ajax.status + ': ' + ajax.statusText);
}

function successResult() {
   // board.innerHTML = ajax.responseText;
    if (restart) {
        setTimeout(request, 1000);
    }
}