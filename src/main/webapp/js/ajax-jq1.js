'use strict'
$(document).ready(function() {
    $('.changes').(function() {
        $.ajax({
            url : 'play',
            data : {
                gameState : gameState,
                isStarted : isStarted,
                resultCode : resultCode,
                winner : winner,
                id : id,
                currentPlayerName : currentPlayerName,
                enemy : enemy},
            success : function(responseText) {
                $('.changes').text(responseText);
                setInterval(function() {
                    refreshBoard()
                }, 1000);
            }
        });
    });
});