'use strict';

function refresh(url, el) {

    var ajax, restart = false;

    function request() {
        ajax = new XMLHttpRequest();

        ajax.open('GET', url, true);
        ajax.send();

        ajax.addEventListener('readystatechange', function () {
            if (ajax.readyState < 4)
                return;

            if (ajax.status != 200) {
                errorResult();
            } else {
                successResult();
            }
        }, false);
    }

    function errorResult() {
        alert(ajax.status + ': ' + ajax.statusText);
    }

    function successResult() {
        el.innerHTML = ajax.responseText;
        if (restart) {
            setTimeout(request, 1000);
        }
    }

    function _start() {
        restart = true;
        request();
    }

    return {
        start: _start
    };
}