
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="3">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/refreshGameList.js"></script>
    <title>List of Games</title>
</head>
<body>

<h1 style="text-align:center"> Game List </h1>

<h2 style="text-align:center"> ${Player.getLogin()} can  either create the game, or join existing one. </h2>



<div style="margin:auto;width:800px" >
    <div style="float: left" class="w3-container">
        <h2>Waiting games</h2>
        <ul>
            <c:forEach items="${listOfWaitingGames}" var="Id">
                <li><a href="/rock-paper-scissors/Waiting?id=${Id}&name=${Player.getLogin()}">
                    <c:out value="Waiting Game №  ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>
    </div>


    <div style="float: left" class="w3-container">
        <h2>Playing games</h2>
        <ul>
            <c:forEach items="${listOfPlayingGames}" var="Id">
                <li><a href="/rock-paper-scissors/play?id=${Id}&name=${Player.getLogin()}">
                    <c:out value="Playing Game №  ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>
    </div>


    <div class="w3-container" style="float: left" >
        <h2>Created games</h2>
        <ul>
            <c:forEach items="${listOfCreatedGames}" var="Id">
                <li><a href="/rock-paper-scissors/created?id=${Id}&name=${Player.getLogin()}">
                    <c:out value="Game number № ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>

    </div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<div class="w3-container" >
    <form method="post" action="create" style="float:left">
        <input type="submit" class="w3-btn" value="Create Game">
    </form> &nbsp; &nbsp;&nbsp; &nbsp;
    <form method="get" action="profile" style="float:left">
        <input type="hidden" name="name" value="${Player.getLogin()}">
        <input type="submit" class="w3-btn" id="profile_btn" value="Profile">
    </form>&nbsp; &nbsp;&nbsp; &nbsp;
    <form method="get" action="logout" style="float:left">
        <input type="submit" class="w3-btn" value="Log out">
    </form>&nbsp; &nbsp; &nbsp; &nbsp;


</div>

</body>
</html>