<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<%@ page import="com.softserveinc.ita.multigame.model.Gender" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta login="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <title>Profile List</title>

</head>

<body>
<h1>Player ${Player.getLogin()}</h1>


<div class="w3-container " style="margin: auto">
    <form method="post" action="update">
        <%
            Player player = (Player) request.getAttribute("Player");

            out.println("<div class=\"w3-container\">Email:</div>");
            out.println(String.format("<input class=\"text\" type=\"text\" name=\"email\" required value =%s>",
                    player.getEmail()));

            out.println("<div class=\"w3-container\">Full name:</div>");
            out.println(String.format("<input class=\"text\" type=\"text\" name=\"fullName\" placeholder=\"full name\" value =%s>",
                    player.getFullName()==null? "":player.getFullName()));

            out.println("<div class=\"w3-container \">Gender:</div>");
            out.println("<div class=\"w3-container \" id=\"gender\">");
            if (player.getGender() == null) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\"> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\"> Female");
            }
            else if (player.getGender() == Gender.MALE) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\" checked> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\"> Female");
            }
            else if (player.getGender() == Gender.FEMALE) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\"> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\" checked> Female");
            }
            out.println("</div>");

            out.println("<div class=\"w3-container \">Birthday:</div>");
            out.println(String.format(  "<div class=\"w3-container \" id=\"birthday\">" +
                            "<input type=\"date\" id=\"date\" name=\"date\" value =%s>" +
                            "</div>",
                    player.getBirthdayDate()==null? "":player.getBirthdayDate()));

            out.println("<div class=\"w3-container \">About:</div>");
            out.println(String.format("<textarea class=\"w3-input \" id=\"about\" name=\"about\" placeholder=\"about\">%s</textarea>",
                    player.getAbout()==null? "":player.getAbout()));

        %>

        <input type="submit" class="w3-btn" value="Save">
    </form>

    </div>

</textarea>

<div class="w3-container ">
    <form method="get" action="gameHistory" style="float:left">
        <input type="submit" class="w3-btn" value="To game History">
    </form>
</div>

</div>
</body>
</html>
