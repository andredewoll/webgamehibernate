<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="3">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <title>Playing Game</title>
 <%--   <script src="js/jquery-3.1.1.js"></script>
    <script src="js/ajax-jq.js"></script>
    <script src="js/ajax-jq1.js"></script>--%>
    <script src="js/jquery-3.1.1.js"></script>
   <%-- <script src="js/ajaxJ.js"></script>--%>
   <%-- <script src="js/main.js"></script>
    <script src="js/ajaxScript.js"></script>--%>
</head>
<body>

<h1  style="text-align:center">RockScissorsPaper game number ${id}</h1>
<h2 style="text-align:center"> <a href="profile?name=${currentPlayerName}">${currentPlayerName}</a> against
   <a href="profile?name=${enemy}" >${enemy}</a></h2>
<h2 class="changes" style="text-align:center">The game is started? -  ${isStarted}</h2>
<h2 class="changes" style="text-align:center">Game info -  ${gameState}</h2>


<div style="text-align:center">
    <form id="turnForm" action="MakeTurn" method="get"  class="w3-container w3-card-4">
        <h3>Choose Your turn, Sir!</h3>
        <input type="radio" name="turn" value="ROCK" class="w3-radio">ROCK&nbsp;&nbsp;
        <input type="radio" name="turn" value="SCISSORS" class="w3-radio">SCISSORS&nbsp;&nbsp;
        <input type="radio" name="turn" value="PAPER" class="w3-radio">PAPER&nbsp;&nbsp;
        <input type="radio" name="turn" value="LIZARD" class="w3-radio">LIZARD&nbsp;&nbsp;
        <input type="radio" name="turn" value="SPOCK" class="w3-radio">SPOCK<br>
        <input type="hidden" id="id" name="id" value="${id}"/>
        <input type="hidden" id="currentPlayerName" name = "currentPlayerName" value="${currentPlayerName}">
        <input type="hidden" id="enemy" name = "enemy" value="${enemy}">
        <input type="hidden" id="winner" name = "winner" value="${winner}">
        <input type="hidden" id="resultCode" name = "resultCode" value="${resultCode}">
        <input type="hidden" id="isStarted" name = "isStarted" value="${isStarted}"> <br>
        <input type="submit" id="makeTurn" value="Make turn" class="w3-btn">
    </form>
</div>


<h2 class="changes" style="text-align:center;color:red" >The Winner is  ${winner}</h2>
<br>


<div class="w3-container w3-center">
    <a href="/rock-paper-scissors/play?id=${id}&name=${currentPlayerName}&enemy=${enemy}">
        <button class="w3-btn">Refresh</button>
    </a>
</div>
<br>
<div class="w3-container w3-center">
    <a href="/rock-paper-scissors/list?id=${id}&name=${currentPlayerName}">
        <button class="w3-btn">Back to games</button>
    </a>
</div>
<div class="w3-container w3-center">
<form method="get" action="gameHistory" style="float:left">
    <input type="submit" class="w3-btn" value="To game History">
</form>
</div>
</body>
</html>