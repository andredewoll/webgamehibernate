package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.util.UtilSessionFactory;
import com.softserveinc.ita.multigame.dao.util.UtilTestingFactoryImpl;
import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;
import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class TestPlayerDaoImpl {

    private PlayerDao playerDao = new PlayerDaoImpl();
    private UtilSessionFactory session = UtilTestingFactoryImpl.getInstance();
    private UtilSessionFactory mockSession = mock(UtilTestingFactoryImpl.class);

    @Before
    public void setUp() {
        try {
            Field fieldPlayerDao = playerDao.getClass().getDeclaredField("sessionFactory");
            fieldPlayerDao.setAccessible(true);
            fieldPlayerDao.set(playerDao, mockSession);
        } catch (Exception nSe) {
            fail("Field \"sessionFactory\" is not accessible");
        }
    }

    @Test
    public void testGetPlayerByID() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player = playerDao.getById(1L);
        assertSame(1L, player.getId());
        verify(mockSession).openSession();
    }

    @Test
    public void testGetPlayerByLogin() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player = playerDao.getByLogin("Alex");
        assertSame("Alex", player.getLogin());
        verify(mockSession).openSession();
    }

    @Test
    public void testGetAllPlayers() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player = playerDao.getByLogin("Alex");
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player2 = playerDao.getByLogin("Andre");
        when(mockSession.openSession()).thenReturn(session.openSession());
        List<Player> players = playerDao.getAll();

        assertSame(true, players.contains(player));
        assertSame(true, players.contains(player2));
        verify(mockSession,times(3)).openSession();
    }


    @Test
    public void testDeletePlayers() {
        Player p = new Player("Bob", "qwerty", "bob@mail");
        when(mockSession.openSession()).thenReturn(session.openSession());
        playerDao.save(p);
        when(mockSession.openSession()).thenReturn(session.openSession());

        assertTrue(playerDao.delete(p));
        verify(mockSession, times(2)).openSession();
    }

    @Test
    public void testUpdatePlayers() {
        Player player = new Player("Andre", "qwerty", "bob@mail");
        when(mockSession.openSession()).thenReturn(session.openSession());
        playerDao.update(player);
        when(mockSession.openSession()).thenReturn(session.openSession());

        assertTrue(playerDao.update(player));
        verify(mockSession, times(2)).openSession();
    }

    @Test
    public void testCantDeleteNull() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        assertFalse(playerDao.delete(null));
        verify(mockSession).openSession();
    }

    @Test
    public void testSavePlayers() {
        Player player = new Player("Bob", "qwerty", "bob@mail");
        when(mockSession.openSession()).thenReturn(session.openSession());
        playerDao.save(player);
        when(mockSession.openSession()).thenReturn(session.openSession());

        assertTrue(playerDao.save(player));
        verify(mockSession, times(2)).openSession();
    }



}
