package com.softserveinc.ita.multigame.dao;


import com.softserveinc.ita.multigame.dao.util.UtilSessionFactory;
import com.softserveinc.ita.multigame.dao.util.UtilTestingFactoryImpl;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;
import org.junit.Before;
import org.junit.Test;


import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestGameHistoryDaoImpl {

    private GameHistoryDao gameDao = new GameHistoryDaoImpl();
    private PlayerDao playerDao = new PlayerDaoImpl();
    private UtilSessionFactory session = UtilTestingFactoryImpl.getInstance();
    private UtilSessionFactory mockSession = mock(UtilTestingFactoryImpl.class);


    @Before
    public void setUp() {
        try {
            Field fieldGameHistory = gameDao.getClass().getDeclaredField("sessionFactory");
            fieldGameHistory.setAccessible(true);
            fieldGameHistory.set(gameDao, mockSession);
            Field fieldPlayerDao = playerDao.getClass().getDeclaredField("sessionFactory");
            fieldPlayerDao.setAccessible(true);
            fieldPlayerDao.set(playerDao, mockSession);
        } catch (Exception nSe) {
            fail("Field \"sessionFactory\" is not accessible");
        }

    }

    @Test
    public void testGameHistoryGetByIdFromDao() {
        when(mockSession.openSession()).thenReturn(session.openSession());

        assertSame(1L, gameDao.getById(1L).getId());
        verify(mockSession).openSession();
    }


    @Test
    public void testGameDaoDoesntSaveNull() {
        when(mockSession.openSession()).thenReturn(session.openSession());

        assertFalse(gameDao.save(null));
        verify(mockSession).openSession();
    }

    @Test
    public void testSavingGameHistory() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player1 = playerDao.getById(1L);
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player2 = playerDao.getById(2L);
        when(mockSession.openSession()).thenReturn(session.openSession());
        GameHistory gameHistory = new GameHistory(1L, player1, player2, LocalDateTime.now(), LocalDateTime.now(),
                new ArrayList<>(), ResultStatus.FIRST_IS_WINNER);

        assertFalse(gameDao.save(gameHistory));
        verify(mockSession, times(3)).openSession();
    }

    @Test
    public void testDeletingGameHistory() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player1 = playerDao.getByLogin("Andre");
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player2 = playerDao.getByLogin("Alex");
        when(mockSession.openSession()).thenReturn(session.openSession());
        GameHistory gameHistory = new GameHistory(2L, player1, player2, LocalDateTime.now(), LocalDateTime.now(),
                new ArrayList<>(), ResultStatus.FIRST_IS_WINNER);

        assertTrue(gameDao.save(gameHistory));
        when(mockSession.openSession()).thenReturn(session.openSession());
        assertFalse(gameDao.delete(gameHistory));
        verify(mockSession, times(4)).openSession();
    }

    @Test
    public void testGettingCreatedGamesFromDao() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player1 = playerDao.getByLogin("Andre");

        when(mockSession.openSession()).thenReturn(session.openSession());
        List<GameHistory> createdGames = gameDao.getBy1PlayerGames(player1);

        assertSame(1, createdGames.size());
        verify(mockSession, times(2)).openSession();
    }

    @Test
    public void testGettingJoinedGamesFromDao() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player1 = playerDao.getByLogin("Andre");
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player2 = playerDao.getByLogin("Alex");

        when(mockSession.openSession()).thenReturn(session.openSession());
        List<GameHistory> createdGames = gameDao.getBy1PlayerGames(player1);
        when(mockSession.openSession()).thenReturn(session.openSession());
        List<GameHistory> joinedGames = gameDao.getBy2PlayerGames(player2);

        assertSame(1, joinedGames.size());
        verify(mockSession, times(4)).openSession();
    }

    @Test
    public void testUpdateGamesHistories() {
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player1 = playerDao.getByLogin("Andre");
        when(mockSession.openSession()).thenReturn(session.openSession());
        Player player2 = playerDao.getByLogin("Alex");
        when(mockSession.openSession()).thenReturn(session.openSession());
        GameHistory gameHistory = new GameHistory(2L, player1, player2, LocalDateTime.now(), LocalDateTime.now(),
                new ArrayList<>(), ResultStatus.FIRST_IS_WINNER);

        assertTrue(gameDao.update(gameHistory));
        verify(mockSession, times(3)).openSession();
    }
}
