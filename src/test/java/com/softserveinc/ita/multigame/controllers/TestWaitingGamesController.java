package com.softserveinc.ita.multigame.controllers;

import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.services.GameListService;

public class TestWaitingGamesController extends Mockito {
	 @Mock
	 private HttpServletRequest request;
	 @Mock
	 private HttpServletResponse response;
	 @Mock
	 private RequestDispatcher dispatcher;
	 @Mock
	 private GameListService gameListServiceService = GameListService.getInstance()  ;


	 private WaitingGamesController controller;

	 @Before
	 public void setUp() throws Exception {
	  MockitoAnnotations.initMocks(this);
	 controller = new WaitingGamesController();
	 }

	 @Test(expected = NullPointerException.class)
	 public void test() throws ServletException, IOException {
		 when(request.getParameter("name")).thenReturn("name");
		 when(request.getParameter("id")).thenReturn("id");
		 when(request.getRequestDispatcher("/WaitingGames.jsp")).thenReturn(dispatcher);
		 when(gameListServiceService.getGameById(1L)).thenReturn(anyObject());
		 when(gameListServiceService.getGameById(1L).getFirstPlayer()).thenReturn(anyObject());
		 when(gameListServiceService.getGameById(1L).getFirstPlayer().getLogin()).thenReturn(anyString());
		 
		 controller.doGet(request, response);

		  verify(request.getParameter("name"));
		 verify(request.getParameter("id"));
		 verify((gameListServiceService.getGameById(1L).getFirstPlayer().getLogin()));
	 }
	 
	 

}
