package com.softserveinc.ita.multigame.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.GameManager;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


public class TestCreateGameController {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private HttpSession session;
    @Mock
    private GameListService gameListServiceService = GameListService.getInstance()  ;
    private CreateGameController controller;
    @Test(expected = NullPointerException.class)
    public void test() throws ServletException, IOException {
        Player player = mock(Player.class);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("Player")).thenReturn(player);

        controller.doPost(request,response);

        verify(request).getSession();
        verify(session).getAttribute("Player");
        verify(response).sendRedirect("list");
    }

}
