package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class TestLogOutController {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private HttpSession session;

    private LogoutController controller;

    @Test(expected = NullPointerException.class)
    public void test() throws ServletException, IOException {

        when(request.getSession()).thenReturn(session);

        controller.doGet(request, response);

        verify(request).getSession();
        verify(session).removeAttribute("Player");
        verify(response).sendRedirect("login");

    }
}
