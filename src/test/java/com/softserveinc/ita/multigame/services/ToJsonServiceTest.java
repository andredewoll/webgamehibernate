package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static org.junit.Assert.assertTrue;

public class ToJsonServiceTest {
    Player player = new Player("Bob","password");
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String json = gson.toJson(player);
    @Test
    public void testToJsonTrue() {
        Gson gson = new Gson();
        assertTrue(player.equals(gson.fromJson(json, Player.class)));
    }
}
