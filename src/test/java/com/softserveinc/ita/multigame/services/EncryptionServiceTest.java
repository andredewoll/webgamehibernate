package com.softserveinc.ita.multigame.services;

import org.junit.Test;
import static org.junit.Assert.*;

public class EncryptionServiceTest {
    EncryptionService encryptionService = EncryptionService.getInstance();
    String password = "sdsadsdfhjghg1323*";

    @Test
    public void testEncryptPasswordTrue() {
        String encryptedPass = encryptionService.encryptPassword(password);

        assertSame(true, encryptionService.checkPassword(password, encryptedPass));
    }

    @Test
    public void testEncryptPasswordFalse() {
        String encryptedPass = encryptionService.encryptPassword(password);

        assertSame(false, encryptionService.checkPassword("anotherPassword", encryptedPass));
    }

}
