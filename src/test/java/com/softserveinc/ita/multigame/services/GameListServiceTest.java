package com.softserveinc.ita.multigame.services;


import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameListServiceTest {

    private GameListService manager;
    private PlayerDao playerDao = new PlayerDaoImpl();
    private GameHistoryDao gameHistoryDao = new GameHistoryDaoImpl();
    private Player player1;
    private Player player2;

    private static Game firstGame;
    private static Game secondGame;

    @Before
    public void setUp() {
        manager = GameListService.getInstance();
        player1 = mock(Player.class, Mockito.CALLS_REAL_METHODS);
        player2 = mock(Player.class, Mockito.CALLS_REAL_METHODS);
        player1.setLogin("player1");
        player2.setLogin("player2");
        when(player1.getLogin()).thenReturn("player1");
        when(player2.getLogin()).thenReturn("player2");
        playerDao = mock(PlayerDaoImpl.class);
        when(playerDao.getByLogin("player1")).thenReturn(player1);
        when(playerDao.getByLogin("player2")).thenReturn(player2);

    }

    @After
    public void tireDown() {
        try {
            Field field = GameListService.class.getDeclaredField("instance");
            field.setAccessible(true);
            field.set(null, null);
        } catch (Exception e) {
            fail("Field \"manager\" is not accessible");
        }
        try {
            Field field = GameListService.class.getDeclaredField("map");
            field.setAccessible(true);
            field.set(null, null);
        } catch (Exception e) {
            fail("Field \"map\" is not accessible");
        }

    }

    @Test
    public void managerIsSingleton() {
        GameListService newManager = GameListService.getInstance();
        assertSame(newManager, manager);
    }

    @Ignore
    @Test
    public void gameIsCreatedWithPlayerNotNull() {
        assertTrue(manager.createGame(player1));
    }

    @Test
    public void gameIsNotCreatedWithPlayerIsNull() {
        assertFalse(manager.createGame(null));
    }

    @Ignore
    @Test()
    public void gameCanBeDeletedIfPlayersWereAdded() {
        manager.createGame(player2);
        assertTrue(manager.deleteGame(1L));
    }

    @Test
    public void newlyCreatedManagerShouldBeEmpty() {
        assertEquals(0, manager.getAllGames().size());
    }

    @Ignore
    @Test
    public void managerContainsAsManyGamesAsHaveBeenAdded() {
        fillManagerWithGames();
        assertEquals(2, manager.getAllGames().size());
    }

    private void fillManagerWithGames() {
        manager.createGame(player1);
        manager.createGame(player2);
    }


}
